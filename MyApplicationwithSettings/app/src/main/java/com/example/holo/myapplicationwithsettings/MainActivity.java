package com.example.holo.myapplicationwithsettings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        PreferenceManager.setDefaultValues(this, R.xml.parametres, false);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        TextView t = (TextView) findViewById(R.id.nomUtilisateur);
        t.setText(sharedPref.getString("nomUtilisateur", "<default>"));

        t = (TextView) findViewById(R.id.civilite);
        t.setText(sharedPref.getString("civilite","Autre"));

        if(sharedPref.getBoolean("darkOn", true))
            getWindow().getDecorView().setBackgroundColor(Color.DKGRAY);
        else
            getWindow().getDecorView().setBackgroundColor(Color.YELLOW);




    }

    //Démarrer activité
    public void goParam(View view)
    {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    //Update l'affichage
    @Override
    protected void onResume() {

        super.onResume();
        PreferenceManager.setDefaultValues(this, R.xml.parametres, false);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        TextView t = (TextView) findViewById(R.id.nomUtilisateur);
        t.setText(sharedPref.getString("nomUtilisateur", "<default>"));

        t = (TextView) findViewById(R.id.civilite);
        t.setText(sharedPref.getString("civilite","Autre"));

        if(sharedPref.getBoolean("darkOn", true))
            setTheme(R.style.DarkTheme);
        else
            setTheme(R.style.AppTheme);


        if(sharedPref.getBoolean("darkOn", true))
            getWindow().getDecorView().setBackgroundColor(Color.DKGRAY);
        else
            getWindow().getDecorView().setBackgroundColor(Color.YELLOW);
    }

}
