package com.example.holo.myapplicationwithsettings;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Console;

public class SettingsActivity extends PreferenceActivity
                                implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals("darkOn")){
            Log.d("-----","ENFIN");
            if(sharedPreferences.getBoolean(key,false)){
                getWindow().getDecorView().setBackgroundColor(Color.DKGRAY);
            }
        }
        if(key.equals("nomUtilisateur"))
            Log.d("New username:", sharedPreferences.getString(key, "<default>"));

    }
}
